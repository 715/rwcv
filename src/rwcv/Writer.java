package rwcv;

class Writer extends MyObject implements Runnable {

    private int id = 0;
    private int wNap = 0; // milliseconds
    private Database db = null;

    Writer(String name, int id, int wNap, Database db) {
        super(name + id);
        this.id = id;
        this.wNap = wNap;
        this.db = db;
        new Thread(this).start();
    }

    public void run() {
        int napping;
        while (true) {
            napping = 1 + (int) random(wNap);
            System.out.println("age=" + age() + ", " + getName()
                    + " napping for " + napping + " ms");
            nap(napping);
            System.out.println("age=" + age() + ", " + getName()
                    + " wants to write");
            db.startWrite(id);
            napping = 1 + (int) random(wNap);
            System.out.println("age=" + age() + ", " + getName()
                    + " writing for " + napping + " ms");
            nap(napping);
            db.endWrite(id);
            System.out.println("age=" + age() + ", " + getName()
                    + " finished writing");
        }
    }
}
