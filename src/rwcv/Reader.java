package rwcv;

class Reader extends MyObject implements Runnable {

    private int id = 0;
    private int rNap = 0; // milliseconds
    private Database db = null;

    Reader(String name, int id, int rNap, Database db) {
        super(name + id);
        this.id = id;
        this.rNap = rNap;
        this.db = db;
        new Thread(this).start();
    }

    public void run() {
        int napping;
        while (true) {
            napping = 1 + (int) random(rNap);
            System.out.println("age=" + age() + ", " + getName()
                    + " napping for " + napping + " ms");
            nap(napping);
            System.out.println("age=" + age() + ", " + getName()
                    + " wants to read");
            db.startRead(id);
            napping = 1 + (int) random(rNap);
            System.out.println("age=" + age() + ", " + getName()
                    + " reading for " + napping + " ms");
            nap(napping);
            db.endRead(id);
            System.out.println("age=" + age() + ", " + getName()
                    + " finished reading");
        }
    }
}