package rwcv;

import java.util.Vector;

class Database extends MyObject {

    private int numReaders = 0;
    private boolean isWriting = false;
    private Vector<MyConcreteObject> waitingReaders = new Vector<MyConcreteObject>();
    private Vector<MyConcreteObject> waitingWriters = new Vector<MyConcreteObject>();

    Database() {
        super("rwDB");
    }

    public synchronized String toString() {
        return getName() + " numR=" + numReaders
                + " numRw=" + waitingReaders.size() + " isW="
                + isWriting + " numWw=" + waitingWriters.size();
    }

    void startRead(int i) {
        MyConcreteObject convey = new MyConcreteObject();
        synchronized (convey) {
            if (cannotReadNow(convey))
                while (true) // wait to be notified, not interrupted
                    try {
                        convey.wait();
                        break;
                    }
                    // notify() after interrupt() race condition ignored
                    catch (InterruptedException ignored) {
                    }
        }
    }

    private synchronized boolean cannotReadNow(MyConcreteObject convey) {
        boolean status;
        System.out.println("  cannotR <: " + this);
        if (isWriting || waitingWriters.size() > 0) {
            waitingReaders.addElement(convey);
            status = true;
        } else {
            numReaders++;
            status = false;
        }
        System.out.println("  cannotR >: " + this);
        return status;
    }

    void startWrite(int i) {
        MyConcreteObject convey = new MyConcreteObject();
        synchronized (convey) {
            if (cannotWriteNow(convey))
                while (true) // wait to be notified, not interrupted
                    try {
                        convey.wait();
                        break;
                    } catch (InterruptedException ignored) {
                    }
        }
    }

    private synchronized boolean cannotWriteNow(MyConcreteObject convey) {
        boolean status;
        System.out.println("  cannotW <: " + this);
        if (isWriting || numReaders > 0) {
            waitingWriters.addElement(convey);
            status = true;
        } else {
            isWriting = true;
            status = false;
        }
        System.out.println("  cannotW >: " + this);
        return status;
    }

    synchronized void endRead(int i) {
        System.out.println("  endR <: " + this);
        numReaders--;
        if (numReaders == 0 && waitingWriters.size() > 0) {
            synchronized (waitingWriters.elementAt(0)) {
                waitingWriters.elementAt(0).notify();
            }
            waitingWriters.removeElementAt(0);
            isWriting = true;
        }
        System.out.println("  endR >: " + this);
    }

    synchronized void endWrite(int i) {
        System.out.println("  endW <: " + this);
        isWriting = false;
        if (waitingReaders.size() > 0) {
            while (waitingReaders.size() > 0) {
                synchronized (waitingReaders.elementAt(0)) {
                    waitingReaders.elementAt(0).notify();
                }
                waitingReaders.removeElementAt(0);
                numReaders++;
            }
        } else if (waitingWriters.size() > 0) {
            synchronized (waitingWriters.elementAt(0)) {
                waitingWriters.elementAt(0).notify();
            }
            waitingWriters.removeElementAt(0);
            isWriting = true;
        }
        System.out.println("  endW >: " + this);
    }
}